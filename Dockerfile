FROM ubuntu:latest

ARG DEBIAN_FRONTEND=noninteractive

RUN apt-get update -y && \
    apt-get install build-essential -y && \
    apt-get install cmake -y && \
    apt-get install libsdl2-dev -y && \
    apt-get install libsdl2-image-dev -y && \
    apt-get install libsdl2-mixer-dev -y

COPY . /usr/src/azrs-tracking

WORKDIR /usr/src/azrs-tracking

RUN bash build.sh 2

WORKDIR /usr/src/azrs-tracking/build/x64/Release

CMD ["./Game"]