#include "precomp.h"
#include "EntityManager.h"
#include "ECS/Entity.h"
#include "Render/Texture.h"
#include <algorithm>

namespace Engine
{
    bool EntityManager::Init()
    {
        return true;
    }

    void EntityManager::Update(float dt)
    {
        // Post-physics update
    }

    void EntityManager::PostGameSpecificUpdate()
    {
        m_Entities.erase(std::remove_if(m_Entities.begin(), m_Entities.end(), [](auto& entity) {
                             return entity->GetIsMarkedForDelete();
                         }),
                         m_Entities.end());
    }

    void EntityManager::AddEntity(Entity* e)
    {
        m_Entities.emplace_back(e);
    }
    void EntityManager::AddEntity(std::unique_ptr<Entity>&& e)
    {
        m_Entities.push_back(std::move(e));
    }

    void EntityManager::RemoveEntity(long int id)
    {
        auto x = std::find_if(m_Entities.begin(), m_Entities.end(), [id](auto& entity) {
            return entity->GetId() == id;
        });

        if (x != m_Entities.end())
        {
            if (Entity* e = x->get())
            {
                e->SetIsMarkedForDelete(true);
            }
        }
    }
} // namespace Engine
