#include "precomp.h"
#include "InputManager.h"

#include "ECS/EntityManager.h"

#include <SDL.h>

namespace Engine
{
    bool KeyDown(KeyboardButton _iKey)
    {
        const unsigned char* state = SDL_GetKeyboardState(nullptr);
        return state[static_cast<int>(_iKey)] != 0;
    }

    bool InputManager::Init()
    {
        LOG_INFO("Initializing InputManager");

        InitKeybinds();

        return true;
    }

    void InputManager::ProcessInput()
    {
        //action key
        for (const std::pair<std::string, int> i : m_InputActions)
        {
            bool bIsPressed = KeyDown(i.second);
            switch (m_InputActionStates[i.first])
            {
            case EInputActionState::None:
            {
                m_InputActionStates[i.first] = bIsPressed ? EInputActionState::JustPressed : EInputActionState::None;
                break;
            }
            case EInputActionState::JustPressed:
            case EInputActionState::Pressed:
            {
                m_InputActionStates[i.first] = bIsPressed ? EInputActionState::Pressed : EInputActionState::Released;
                break;
            }
            case EInputActionState::Released:
            {
                m_InputActionStates[i.first] = bIsPressed ? EInputActionState::JustPressed : EInputActionState::None;
                break;
            }
            default:
                ASSERT("Unknown EInputActionState {0}", m_InputActionStates[i.first]);
                m_InputActionStates[i.first] = EInputActionState::None;
                break;
            }
        }
    }

    void InputManager::Update(float dt, EntityManager* entityManager)
    {
        ProcessInput();

        // Update entities
        auto inputComponents = entityManager->GetAllComponentInstances<InputComponent>();

        for (auto component : inputComponents)
        {
            for (auto& action : component->inputActions)
            {
                action.m_Active = IsButtonActionActive(action.m_Action, action.m_ActionTriggerState);
                action.m_JustPressed = IsButtonActionActive(action.m_Action, EInputActionState::JustPressed);
            }
        }
    }

    void InputManager::Shutdown()
    {
        m_InputActions.clear();
        m_InputActionStates.clear();
    }

    bool InputManager::IsButtonActionActive(EInputAction _eAction, EInputActionState _eState) const
    {
        ASSERT(m_InputActionStates.find(_eAction) != m_InputActionStates.end(), "Unknown input action: {}", _eAction);
        return m_InputActionStates.at(_eAction) == _eState;
    }

    void InputManager::InitKeybinds()
    {
        m_InputActionStates.clear();
        m_InputActions.clear();

        //changed some inputs
        m_InputActions["PlayerShootUp"] = SDL_SCANCODE_UP;
        m_InputActions["PlayerShootLeft"] = SDL_SCANCODE_LEFT;
        m_InputActions["PlayerShootDown"] = SDL_SCANCODE_DOWN;
        m_InputActions["PlayerShootRight"] = SDL_SCANCODE_RIGHT;
        m_InputActions["PauseGame"] = SDL_SCANCODE_P; //VK_ESCAPE;
        m_InputActions["RestartGame"] = SDL_SCANCODE_R;
        m_InputActions["PlayerMoveUp"] = SDL_SCANCODE_W;
        m_InputActions["PlayerMoveLeft"] = SDL_SCANCODE_A;
        m_InputActions["PlayerMoveDown"] = SDL_SCANCODE_S;
        m_InputActions["PlayerMoveRight"] = SDL_SCANCODE_D;
        m_InputActions["PanCameraUp"] = 'Y';
        m_InputActions["PanCameraLeft"] = 'Y';
        m_InputActions["PanCameraDown"] = 'Y';
        m_InputActions["PanCameraRight"] = 'Y';
        m_InputActions["Start"] = SDL_SCANCODE_RETURN;
        m_InputActions["Quit"] = SDL_SCANCODE_ESCAPE;
        m_InputActions["Reset"] = SDL_SCANCODE_R;
    }

    bool InputManager::IsActionActive(InputComponent* inputComponent, EInputAction targetAction)
    {
        auto found = std::find_if(
            std::begin(inputComponent->inputActions),
            std::end(inputComponent->inputActions),
            [targetAction](Engine::InputAction e) {
                return e.m_Action == targetAction && e.m_Active == true;
            });

        return found != std::end(inputComponent->inputActions);
    }

    bool InputManager::WasJustPressed(InputComponent* inputComponent, EInputAction targetAction)
    {
        auto found = std::find_if(
            std::begin(inputComponent->inputActions),
            std::end(inputComponent->inputActions),
            [targetAction](Engine::InputAction e) {
                return e.m_Action == targetAction && e.m_JustPressed == true;
            });

        return found != std::end(inputComponent->inputActions);
    }
} // namespace Engine
