#pragma once

namespace Game
{
    enum EHUDBuffType
    {
        SpeedBuff = 0,
        RapidFire,
        Apocalypse,
        TimeoutBuff,
        TrippleShot,
        MultiShot,
        Count,
        Inalid
    };

    struct DemageComponent : public Engine::Component
    {
        int demage = 100;
    };

    struct HUDBuffIconComponent : public Engine::Component
    {
        EHUDBuffType m_BuffType{EHUDBuffType::Inalid};
    };
} // namespace Game