#pragma once
#include <precomp.cpp>
#include "Entities/Misc.h"

namespace Engine
{
    class EntityManager;
    struct Texture;
} // namespace Engine

namespace Game
{
    class PauseController
    {
    private:
        Engine::Texture* m_texture;

    public:
        bool Init(Engine::EntityManager* entityManager_, int window_width, int window_height, Engine::Texture* texture);
        bool ShouldExit(Engine::EntityManager* entityManager_);
        bool ShouldReset(Engine::EntityManager* entityManager_);
    };
} // namespace Game
