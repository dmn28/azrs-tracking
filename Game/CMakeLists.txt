cmake_minimum_required(VERSION 3.13)
project(Game VERSION 1.0.0)

set(PROJECT_NAME Game)
set(CMAKE_CXX_STANDARD 20)
set(CMAKE_CXX_FLAGS_DEBUG "-Wall -Wextra --coverage")

include(FindPkgConfig)
find_package(SDL2 REQUIRED)
pkg_check_modules(SDL2_Mixer REQUIRED IMPORTED_TARGET SDL2_mixer)
pkg_check_modules(SDL2_image REQUIRED IMPORTED_TARGET SDL2_image)
include_directories(${SDL2_INCLUDE_DIRS} ${SDL2_IMAGE_INCLUDE_DIRS})

################################################################################
# Source groups
################################################################################
set(src
    "src/PauseController.cpp"
    "src/PauseController.h"
    "src/precomp.cpp"
    "src/precomp.h"
)
source_group("src" FILES ${src})

set(src__Core
    "src/Core/GameApp.cpp"
    "src/Core/GameApp.h"
)
source_group("src\\Core" FILES ${src__Core})

set(src__Entities
    "src/Entities/AudioController.h"
    "src/Entities/BorderController.cpp"
    "src/Entities/BorderController.h"
    "src/Entities/CameraController.cpp"
    "src/Entities/CameraController.h"
    "src/Entities/FireballController.cpp"
    "src/Entities/FireballController.h"
    "src/Entities/HudController.cpp"
    "src/Entities/HudController.h"
    "src/Entities/ItemsController.cpp"
    "src/Entities/ItemsController.h"
    "src/Entities/Misc.h"
    "src/Entities/NPC/EnemyAnimation.cpp"
    "src/Entities/NPC/EnemyAnimation.h"
    "src/Entities/ObstacleController.cpp"
    "src/Entities/ObstacleController.h"
    "src/Entities/PlayerController.cpp"
    "src/Entities/PlayerController.h"
    "src/Entities/StageController.cpp"
    "src/Entities/StageController.h"
)

source_group("src\\Entities" FILES ${src__Entities})

set(src__Entities__Components
    "src/Entities/Components/GameSpecificComponents.h"
)

source_group("src\\Entities\\Components" FILES ${src__Entities__Components})

set(src__Entities__Dummy
    "src/Entities/Dummy/DummyController.cpp"
    "src/Entities/Dummy/DummyController.h"
    "src/Entities/Dummy/DummySpecificEntities.h"
)

source_group("src\\Entities\\Dummy" FILES ${src__Entities__Dummy})

set(src__Entities__NPC
    "src/Entities/NPC/Controllers/EarthNPCController.cpp"
    "src/Entities/NPC/Controllers/EarthNPCController.h"
    "src/Entities/NPC/Controllers/FireNPCController.cpp"
    "src/Entities/NPC/Controllers/FireNPCController.h"
    "src/Entities/NPC/Controllers/MentalNPCController.cpp"
    "src/Entities/NPC/Controllers/MentalNPCController.h"
    "src/Entities/NPC/Controllers/WaterNPCController.cpp"
    "src/Entities/NPC/Controllers/WaterNPCController.h"
    "src/Entities/NPC/Controllers/WindNPCController.cpp"
    "src/Entities/NPC/Controllers/WindNPCController.h"
    "src/Entities/NPC/EnemiesFactory.cpp"
    "src/Entities/NPC/EnemiesFactory.h"
    "src/Entities/NPC/EnemyController.cpp"
    "src/Entities/NPC/EnemyController.h"
    "src/Entities/NPC/EnemySpecificEntities.h"
)

source_group("src\\Entities\\NPC" FILES ${src__Entities__NPC})

set(ALL_FILES
    ${src}
    ${src__Core}
    ${src__Entities}
    ${src__Entities__Components}
    ${src__Entities__Dummy}
    ${src__Entities__NPC}
)

################################################################################
# Target
################################################################################
add_executable(${PROJECT_NAME} ${ALL_FILES})

use_props(${PROJECT_NAME} "${CMAKE_CONFIGURATION_TYPES}" "${DEFAULT_CXX_PROPS}")
set(ROOT_NAMESPACE Game)

################################################################################
# Include directories
################################################################################
target_include_directories(${PROJECT_NAME} PUBLIC
    "${CMAKE_CURRENT_SOURCE_DIR};"
    "${CMAKE_CURRENT_SOURCE_DIR}/src;"
)

################################################################################
# Dependencies
################################################################################
add_dependencies(${PROJECT_NAME}
    Engine
)

# Link with other targets.
target_link_libraries(${PROJECT_NAME} PRIVATE
    Engine
)
target_link_libraries(${PROJECT_NAME} PRIVATE "${SDL2_LIBRARIES}")
target_link_libraries(${PROJECT_NAME} PRIVATE "${SDL2IMAGE_LIBRARIES}")
target_link_libraries(${PROJECT_NAME} PRIVATE m)

