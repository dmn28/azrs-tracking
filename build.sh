mkdir -p build 
cd build
echo "Choose build type:"
echo "1. Debug"
echo "2. Release"
conf="None"
if [ $# != 0 ]  && [ $1 == "2" ]
then 
varname="2"
else
read varname
fi
if [ $varname == "1" ]
then
cmake -DCMAKE_BUILD_TYPE=Debug ./..
conf="Debug"
elif [ $varname == "2" ]
then
cmake -DCMAKE_BUILD_TYPE=Release ./.. 
conf="Release"
else
echo "Wrong build type! Enter [1|2]"
exit 1
fi

make
cd ..
cp ./Game/Data ./build/x64/$conf -r
