echo "Generating documentation for Engine:"
cd Engine 
mkdir Documentation
doxygen engine_doc.config
cd ..
echo "Generating documentation for Game:"
cd Game
mkdir Documentation
doxygen game_doc.config
echo "**DONE**"
echo "Generation of documentation is finished"
echo "Documentation is generated in Documentation folder inside project folders"
echo "To open documentation just open index.html file, which is located in html folder, in your web browser"
