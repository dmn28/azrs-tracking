cd build/Engine/CMakeFiles/Engine.dir
lcov -c -d .. -o engine_coverage.cov
genhtml -o ../../../../coverage/engine_coverage/ engine_coverage.cov
echo "Coverage report has been generated for engine. You can check it in coverage folder."
cd ../../../Game/CMakeFiles/Game.dir
lcov -c -d .. -o game_coverage.cov
genhtml -o ../../../../coverage/game_coverage/ game_coverage.cov
echo "Coverage report has been generated for game. You can check it in coverage folder."
echo "Do you want to delete gcov data file now? [y/n]"
read answer
if [ $answer == "y" ]
then
lcov -z -d .
cd ../../../Engine/CMakeFiles/Engine.dir
lcov -z -d .
echo "Gcov data has been deleted"
fi