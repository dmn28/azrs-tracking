# Magemaggedon
Gasp! Earths elementals have gone MAD. The most peace-loving spiritual creatures have gone berserk, and no one knows why. Luckly a great mage has risen up to find the cause of this change. He will return the earth to what it once was or perish trying! <br>
Trailer & Gameplay: https://youtu.be/bQW73hVuHJ0

This is a fork from original project [Magemageddon](https://github.com/PetarZecevic97/Paint-the-town-crimson).

The goal of the project is to make possible to play the game on Linux platform and to test our knowledge of software development tools.

Team: Aleksa Voštić, Stefan Isailović, Damjan Đorić

## Build
### [0] Requirements
Before building, you need to set up:
* sudo apt get install libsdl2-dev
* sudo apt get install libsdl2-mixer-dev
* sudo apt get install libsdl2-image-dev
* sudo apt get install cmake

### [1] Building
To build a game, just run `bash build.sh` script. <br>

### [2] Run&Play
To run the game, position yourself in `build/x64/[Debug | Release]` and then run `./Game`.
